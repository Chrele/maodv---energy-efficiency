# Modified AODV on Energy Consumption Efficiency #

Author     : Christopher Andrew  
Student ID : 05111640000107  
Class      : Wireless Networking  

### Definition: ###
   This modification of AODV is located on the 
route discovery phase. On the AODV, the souce 
node will boradcast hello message with RREQ 
table in it. This RREQ table will be modified 
by adding an extra field containing the Minimum
Energy Required Threshold(MERT).

### Abstract ###

### Introduction ###

#### Background Problems ####
1. Inefficient energy consumption in the
   original AODV.
2. The neglected usage of energy called
   Residual Energy.
3. The high probability of the broken link b
   events caused by dead nodes while
   transmitting data packet.

### Theoretical Background ###
#### AODV ####
	An Ad Hoc On-Demand Distance Vector (AODV) is a routing protocol designed for wireless and mobile ad hoc networks. This protocol establishes routes to destinations on demand and supports both unicast and multicast routing. The AODV protocol was jointly developed by Nokia Research Center, the University of California, Santa Barbara and the University of Cincinnati in 1991.
	The AODV protocol builds routes between nodes only if they are requested by source nodes thus it is called on-demand algorithm and does not create any extra traffic for communication along links. The routes are maintained as long as they are required by the sources. They also form trees to connect multicast group members. AODV makes use of sequence numbers to ensure route freshness. They are self-starting and loop-free besides scaling to numerous mobile nodes. 
	AODV has three main phases:
	1. Route Discovery
	2. Route Maintenance
    3. Packet Data Transmition

##### AODV Route Discovery Phase #####
In this phase of AODV, the Source Node broadcasts an hello message together with a RREQ(Route-Request) table. <<TBC>>

#### Advantages of using AODV ####
	1. The routes are established on demand
	2. Destination sequence numbers are used to find the latest route to destination.
	3. The connection setup delay is taking less time.
    4. AODV can respond very quickly to the topological changes which affect the active routes.
    5. High adaptability
	6. Supports unicast and multicast packet transmission.
	7. AODV doesn't put any additional overheads on the data packets it send.
    8. Categorized as a flat routing protocol, which mean that it doesn'y need any central administrative system.
	9. Loop-free, self-starting, and capable to handle high number of nodes.

#### Disadvantages of using AODV ####
	1. There are many control packets generated when a broken link event occurs.
	2. Has high processing demand
	3. Occupy a large share of bandwith
	4. Takes more time to build the routing table.
	5. As the size of the network grows, various performance metrics will decreased.


### Proposed Modification ###

#### Detailed Method ####
    The modification of the original AODV has :
	1. The RREQ table is modified by adding one
       extra field containing the Minimum Energy    
       Required Threshold(MERT).
    2. The receiving nodes will compare its own
       residual energy and the MERT contained in
       the RREQ tables. If its residual energy id
       equal or more than the MERT, the RREQ will 
       be accepted and re-broadcasted.
	   Otherwise, the RREQ will be dropped.

#### Advantages of using Modified AODV ####
	There are several advantages:
	1. The number of multi-routes received by the
       source node before sending the packet data 
       is minimalized due to the dropped RREQ if 
       the receiving node's residual energy is
       less than the MERT.
    2. The probability of a broken link cause by
       dead nodes in AODV will be minimalized as
       every nodes used as the data route will be 
       guaranteed to have energy equal or more 
       than the MERT.
	3. The efficiency level of energy consumption
       in AODV, especially on the Route Discovery 
       will be increased.
	

### Processes and Implementations ###
#### System Description ####
	The Implementation of the Modified AODV is done 
	in Linux Debian Stretch 9.0 environment and the 
	simulation is done with the Network 
	Simulator(NS)2 version 2.35
	
#### PseudoCode ####
    The files are located at https://drive.google.com/open?id=1dSTexU4hQ1OebddMJOmttE1UZlDhYg2J
    the files are uploaded to the google Drive because the GitLab server error(cannot upload to GitLab)

### References ###
https://www.techopedia.com/definition/2922/ad-hoc-on-demand-distance-vector-aodv
https://www.researchgate.net/figure/AODV-Advantages-and-Disadvantages_tbl1_235047709

